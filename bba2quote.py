#!/usr/bin/python

import sys
import re

RES = re.compile('(?P<unix_time>unixTime=.*?\s\s)(.*conType=(?P<con_type>.*?)\s)(.*sym=(?P<symbol>.*?)\s)')
STRIKE = re.compile('(Strike=((\d|\s|\-)*))')
BIDPRICE = re.compile('Bid=((\d|\s|\-)*) ')
BIDSIZE = re.compile('BidSiz=((\d|\s)*) ')
ASKPRICE = re.compile('Ask=((\d|\s|\-)*) ')
ASKSIZE = re.compile('AskSiz=((\d|\s)*) ')
OUT_TEMPLATE = """msgType=QuoteMsg conType=%s exch=202 sym=%s                 fast=normal Quote=%s TckSiz=%s SalesCond=%s   """

for line in sys.stdin:
    if 'QuoteMsg' in line:
        if 'SalesCond=Ask' in line or 'SalesCond=Bid' in line:
            sys.stdout.write(line)
            continue
    if 'BidAskMsg' in line:
        res = RES.search(line)
        unix_time = res.group('unix_time')
        instrument = res.group('symbol')
        con_type = res.group('con_type')
        if con_type in ['put', 'call']:
            instrument += 5 * ' ' + STRIKE.search(line).group(1)
        if 'Bid=013' not in line:
            bid_price, bid_size = BIDPRICE.search(line).group(1), BIDSIZE.search(line).group(1)
            sys.stdout.write(unix_time + OUT_TEMPLATE % (con_type, instrument, bid_price, bid_size, 'Bid'))
        if 'Ask=013' not in line:
            ask_price, ask_size = ASKPRICE.search(line).group(1), ASKSIZE.search(line).group(1)
            sys.stdout.write(unix_time + OUT_TEMPLATE % (con_type, instrument, ask_price, ask_size, 'Ask'))

