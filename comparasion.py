import os
import glob
import subprocess
import re
import difflib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


class Prepare:
    def __init__(self, ihepr_path, old_folder='', new_folder='', result_folder=''):
        self.ihepr_path = ihepr_path
        self.folders = {'NEW': new_folder, 'OLD': old_folder}
        self.result_folder = result_folder

    def unzip(self, folder, archive_type=''):
        run_params = ['', '-d', '']
        if not archive_type:
            archive_type = os.path.basename(os.listdir(folder)[0]).split('.')[-1]
        if archive_type == 'Z':
            run_params[0] = 'gzip'
        elif archive_type == 'bz2':
            run_params[0] = 'bzip2'
        files = glob.glob('%s/*.%s' % (folder, archive_type))
        for file in files:
            run_params[2] = file
            subprocess.call(run_params)

    def raw_cat(self, folder, output_name):
        subprocess.call('cat %s/*.raw > %s/%s' % (folder, folder, output_name), shell=True)

    def to_text(self, folder, input_name):
        out_path = '%s/%s_text' % (folder, input_name)
        if not os.path.exists(out_path):
            subprocess.call(self.ihepr_path + ' %s/%s' % (folder, input_name) + '> %s' % out_path , shell=True)
        return out_path

    def categories(self, input_file_path):
        return Messages(None, None).uniq_(input_file_path, 'conType=(.*?) ')

    def cut(self, input_file_path, output_dir, categories=('future', 'call', 'put', 'cash', 'strategy', 'equity', 'bond', 'noContract')):
        output_files = {el: open(os.path.join(output_dir, os.path.basename(input_file_path) + '_%s' % el), 'w+') for el
                        in categories}
        with open(input_file_path) as input_file:
            for line in input_file:
                category = re.search('conType=(.*?) ', line)
                if category:
                    category = category.group(1)
                    if category not in categories:
                        print 'conType %s not in categories list' % category
                    elif not 'Refresh' in line:
                        output_files[category].write(line)
        out = {}
        for k in output_files:
            out[k] = output_files[k].name
            output_files[k].close()
        return out
    

    def prepare(self, unzip=True):
        out = {'NEW':[], 'OLD':[]}
        for f in self.folders:
            if unzip:
                self.unzip(self.folders[f])
            self.raw_cat(self.folders[f], f)
            text_file_path = self.to_text(self.folders[f], f)
            categories = self.categories(text_file_path)
            files = self.cut(text_file_path, self.result_folder, categories)
            out[f] = files
        return Output(out)


class Output:
    def __init__(self, result_files):
        self.result_files_old = result_files['OLD']
        self.result_files_new = result_files['NEW']

    def instruments(self, category):
        return Instruments(self.result_files_old[category], self.result_files_new[category], pattern='sym=(.*?) ', category=category)

    def quote_types(self, category):
        return Quote_types(self.result_files_old[category], self.result_files_new[category], pattern='msgType=(.*?) ', category=category)

    def categories(self):
        return Categories(self.result_files_old, self.result_files_new, category='categories')

    def stat(self, category):
        return Statistics(self.result_files_old[category], self.result_files_new[category], category=category)

    def old(self, category):
        return Statistics(self.result_files_old[category], category=category)

    def new(self, category):
        return Statistics(self.result_files_new[category], category=category)





class Compare:
    def __init__(self, old, new, category):
        self.old = old
        self.new = new
        self.category = category

    def compare(self):
        return difflib.unified_diff(self.old, self.new, fromfile='OLD %s' % self.category, tofile='NEW %s' % self.category)

    def result(self):
        print '=== COMPARING %s ===' % self.category
        r = self.compare()
        for line in r:
            print line

class Messages:
    def __init__(self,  result_files_old, result_files_new, pattern=None, category=None):
        self.result_files_old = result_files_old
        self.result_files_new = result_files_new
        self.pattern = pattern
        self.category = category

    def uniq_(self, file_path, pattern):
        out = set()
        with open(file_path) as f:
            for line in f:
                matched = re.search(pattern, line)
                if matched:
                    matched = matched.group(1)
                    if matched in out:
                        continue
                    out.add(matched)
        return list(out)

    def old(self):
        return self.uniq_(self.result_files_old, self.pattern)

    def new(self):
        return self.uniq_(self.result_files_new, self.pattern)

    def compare(self):
        return Compare(self.old(), self.new(), self.category)


class Instruments(Messages):
    pass


class Quote_types(Messages):
    pass


class Categories(Messages):
    def old(self):
        return [k for k in self.result_files_old]

    def new(self):
        return [k for k in self.result_files_new]


class Statistics:
    def __init__(self, result_file, category):
        self.result_file = result_file
        self.category = category

    def count(self, quote_type, instrument=None):
        counter = 0
        with open(self.result_file) as f:
            for line in f:
                if quote_type in line:
                    if instrument:
                        if instrument in line:
                            counter += 1
                    else:
                        counter += 1
        return counter

    def get_prices(self, quote_type, instrument, side):
        power = {3:0, 4:1, 5:2, 6:3, 7:4, 8:5, 9:6, 10:7, 11:8, 12:9, 13:10}
        times = []
        prices = []
        time_pattern = ' \d{6} \d{6}'
        if quote_type == 'BidAskMsg':
            if side == 'Bid':
                price_pattern = 'Bid=(.+?)([A-Za-z]|  $)'
            elif side == 'Ask':
                price_pattern = 'Ask=(.+?)([A-Za-z]|  $)'
        elif quote_type == 'QuoteMsg':
            price_pattern = 'Quote=(.*) TckSiz'
        with open(self.result_file) as f:
            for line in f:
                if instrument in line and quote_type in line and side in line:
                    price = re.search(price_pattern, line).group(1).split()
                    time = re.search(time_pattern, line).group(0).split()
                    price = float(price[1])/(10**power[int(price[0])])
                    time = int(time[0]+time[1])
                    prices.append(price)
                    times.append(time)
        return (times, prices)

def plot(x, y, xlim=[], ylim=[], descript='', save_to=''):

    if xlim:
        plt.xlim(xlim)
    if ylim:
        plt.ylim(ylim)
    if descript:
        plt.title(descript)

    plt.plot(x, y)
    if save_to:
        plt.savefig(save_to)
    else:
        plt.show()


if __name__ == '__main__':
    old_folder = '/mnt/c/old'
    new_folder = '/mnt/c/new'
    ihepr_path = '/mnt/c/WRK/he1.3.02.00/ihepr'
    result_folder = '/mnt/c/result'
    ow = Prepare(ihepr_path, old_folder, new_folder, result_folder)
    output = ow.prepare(unzip=False)
    categories = output.categories()
    categories.compare().result()


    old_categories = categories.old()
    new_categories = categories.new()
    for cat in set(old_categories).intersection(set(new_categories)):
        output.instruments(category=cat).compare().result()
        output.quote_types(category=cat).compare().result()

    old_bidask_future_count = output.new(category='future').count('BidAskMsg')
    winz16_bidask_prices = output.new(category='future').get_prices('BidAskMsg', 'WINZ16', 'Ask')
    #print 'WINZ16 Ask quotes from BidAskMsgs:', winz16_bidask_prices
    for i in range(max(len(winz16_bidask_prices[0]), len(winz16_bidask_prices[1]))):
        print winz16_bidask_prices[0][i], winz16_bidask_prices[1][i]
    plot(winz16_bidask_prices[0][:-1], winz16_bidask_prices[1][:-1], save_to='/mnt/c/result/plot.png')



"""




old_folder = '/mnt/c/old'
new_folder = '/mnt/c/new'
ihepr_path = '/mnt/c/WRK/he1.3.02.00/ihepr'
result_folder = '/mnt/c/result'
ow = Prepare(ihepr_path, old_folder, new_folder, result_folder)
output = ow.prepare(unzip=False)

future_instruments = output.instruments(category='future')
future_instruments.compare().result()
old_future_instruments = future_instruments.old()
new_future_instruments = future_instruments.new()



quotes = output.quote_types(category='future')
quotes.compare().result()

categories = output.categories()
categories.compare().result()
print categories.old()

fut_bidask_count = output.new(category='future').count('BidAskMsg')
print fut_bidask_count
winz16_bidask_prices = output.new(category='future').get_prices('BidAskMsg', 'WINZ16', 'Ask')
print winz16_bidask_prices
#output.stat(category='future').count(quote_type='BidAskMsg', instrument='WINZ16')
#fut_bid_prices = output.stat(category='future').prices(side='Bid', instrument='WINZ16')



instruments_report.print()
instruments_report.save()

categories_report = output.categories.compare()
categories_report.print()
categories_report.save()

quotes_price = output.get_quotes(instrument='WINZ16').old

"""