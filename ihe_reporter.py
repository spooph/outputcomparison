import re
import os
import sys
from optparse import OptionParser
import json
import hashlib


ACCEPTED_TYPES = ('QuoteMsg', 'BidAskMsg', 'MarketDepthMsg', 'volOrOI', 'SettleMsg', 'hiLo', 'InstStatusMsg',
                  'hiLoLast')
SCALES = {i: 10**(i-3) for i in range(3, 15)}
msg_type_ptrn = re.compile('msgType=(.*?)\s')
symbol_ptrn = re.compile('sym=(.*?)\s')
con_type_ptrn = re.compile('conType=(.*?)\s')
strike_ptrn = re.compile('Strike=((\d|\s|\-)*)\s')
sales_cond_ptrn = re.compile('SalesCond=(.*?)\s')
voitype_ptrn = re.compile('voiType=(.*?)\s')


def decimal_price(scale, price):
    return int(price) / SCALES[int(scale)]


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(16384), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


class Reporter:
    def __init__(self, old_ihe_path, new_ihe_path):
        self.old_ihe_path = old_ihe_path
        self.new_ihe_path = new_ihe_path
        self.md5_old, self.md5_new = md5(self.old_ihe_path), md5(self.new_ihe_path)
        if not self.restore():
            self.old_total_stat = self.get_total_stat(old_ihe_path)
            self.new_total_stat = self.get_total_stat(new_ihe_path)
            self.dump()
        self.diff_format = '%-5s %-12s %-12s\n'
        self.stat_format = '%-5s %-20s %-20s\n'
        self.default_dump_name = {'old': 'olddump.json', 'new': 'newdump.json'}

    @staticmethod
    def get_total_stat(path):
        unrecognized = list()
        total_stat = dict()
        with open(path) as f:
            for line in f:
                if 'Refresh' in line:
                    continue
                msg_type = msg_type_ptrn.search(line).group(1)
                if msg_type in ACCEPTED_TYPES:
                    if msg_type == 'QuoteMsg':
                        msg_type += sales_cond_ptrn.search(line).group(1)
                    if msg_type == 'volOrOI':
                        msg_type += voitype_ptrn.search(line).group(1)

                    symbol = symbol_ptrn.search(line).group(1)
                    con_type = con_type_ptrn.search(line).group(1)
                    if con_type in ('Call', 'Put'):
                        strike = strike_ptrn.search(line).group(1)
                        symbol += '_strike_%f' % decimal_price(*strike.split())

                    if not total_stat.get(con_type):
                        total_stat[con_type] = {}
                    if not total_stat[con_type].get(symbol):
                        total_stat[con_type][symbol] = {}
                    if not total_stat[con_type][symbol].get(msg_type):
                        total_stat[con_type][symbol][msg_type] = 0
                    total_stat[con_type][symbol][msg_type] += 1
                elif msg_type != 'text':
                    if msg_type not in unrecognized:
                        unrecognized.append(msg_type)
                else:
                    continue
        print('Unrecognized types: ', ','.join(unrecognized))
        return total_stat

    @staticmethod
    def get_categories(total_stat):
        return total_stat.keys()

    @staticmethod
    def get_insruments_by_cat(total_stat, category):
        try:
            return total_stat[category].keys()
        except KeyError:
            return []

    def diff_printer(self, set_old, set_new):
        output = self.diff_format % (' ', 'OLD', 'NEW')
        output += self.diff_format % (' ', '---', '---')
        all_elements = sorted(set_old | set_new)
        old = [el if el in set_old else '*' for el in all_elements]
        new = [el if el in set_new else '*' for el in all_elements]
        for i in range(len(new)):
            output += self.diff_format % (' ', old[i], new[i])
        return output

    def stat_printer(self, old, new):
        output = self.stat_format % (' ', 'OLD', 'NEW')
        output += self.stat_format % (' ', '---', '---')
        all_elements = sorted(set(old.keys()) | set(new.keys()))
        old = ['%s: %i' % (el, old[el]) if el in old.keys() else '*' for el in all_elements]
        new = ['%s: %i' % (el, new[el]) if el in new.keys() else '*' for el in all_elements]
        for i in range(len(new)):
            output += self.stat_format % (' ', old[i], new[i])
        return output

    def compare_categories(self):
        old_cats, new_cats, _ = self.old_new_common_cats()
        output = 10*'=' + '\nCompare categories:\n'
        output += self.diff_printer(old_cats, new_cats)
        output += 'Categories in NEW, but not in OLD: %s\n' % ','.join(new_cats.difference(old_cats))
        output += 'Categories in OLD, but not in NEW: %s\n' % ','.join(old_cats.difference(new_cats))
        print(output)

    def old_new_common_cats(self):
        new_cats = set(self.get_categories(self.new_total_stat))
        old_cats = set(self.get_categories(self.old_total_stat))
        common_cats = new_cats.intersection(old_cats)
        return old_cats, new_cats, common_cats

    def old_new_common_instr(self, cat):
        old_instr = set(self.get_insruments_by_cat(self.old_total_stat, cat))
        new_instr = set(self.get_insruments_by_cat(self.new_total_stat, cat))
        common_instr = new_instr.intersection(old_instr)
        return old_instr, new_instr, common_instr

    def compare_instr_by_category(self):
        output = 10*'=' + '\nCompare instrumentsby category:\n'
        old_cats, new_cats, common_cats = self.old_new_common_cats()
        for cat in sorted(common_cats):
            output += '===> %s\n' % cat
            old_instr, new_instr, _ = self.old_new_common_instr(cat)
            output += self.diff_printer(old_instr, new_instr)
            output += 'Instruments in NEW, but not in OLD: %s\n' % ','.join(new_instr.difference(old_instr))
            output += 'Instruments in OLD, but not in NEW: %s\n' % ','.join(old_instr.difference(new_instr))
        print(output)

    def compare_stat(self):
        output = 10*'=' + '\nCompare instruments stats by category:\n'
        old_cats, new_cats, common_cats = self.old_new_common_cats()
        for cat in sorted(common_cats):
            output += '===> %s\n' % cat
            _, _, common_instr = self.old_new_common_instr(cat)
            for instr in common_instr:
                output += 'INSTRUMENT %s\n' % instr
                output += self.stat_printer(self.old_total_stat[cat][instr], self.new_total_stat[cat][instr])
        print(output)

    def dump(self):
        try:
            with open(self.md5_old, 'w') as old:
                with open(self.md5_new, 'w') as new:
                    old_dump = json.dumps(self.old_total_stat)
                    new_dump = json.dumps(self.new_total_stat)
                    old.write(old_dump)
                    new.write(new_dump)
        except Exception:
            print('Can not save dump')

    def restore(self):
        try:
            with open(self.md5_new) as nf:
                with open(self.md5_old) as of:
                    self.old_total_stat = json.load(of)
                    self.new_total_stat = json.load(nf)
                    return True
        except Exception:
            print('Can not restore json objects')

    @staticmethod
    def stat_calc(instr_dict):
        all_msg_types = {msg_type: 0 for instr in instr_dict for msg_type in instr_dict[instr]}
        for instr in instr_dict:
            for msg_type in instr_dict[instr]:
                all_msg_types[msg_type] += instr_dict[instr][msg_type]
        return all_msg_types

    def total_counters(self):
        output = 10 * '=' + '\nStats by category:\n'
        old_cats, new_cats, common_cats = self.old_new_common_cats()
        for cat in old_cats.union(new_cats):
            output += '===> %s\n' % cat
            try:
                old_stat = self.stat_calc(self.old_total_stat[cat])
            except KeyError:
                old_stat = {}
            try:
                new_stat = self.stat_calc(self.new_total_stat[cat])
            except KeyError:
                new_stat = {}
            output += self.stat_printer(old_stat, new_stat)
        print(output)


def main():
    optparser = OptionParser(usage="%prog [options]")

    optparser.add_option("-o", "--oldihe", help="path to OLD IHE textfile",
                         metavar="FILE", default=None)

    optparser.add_option("-n", "--newihe", help="path to NEW IHE textfile",
                         metavar="FILE", default=None)
    optparser.add_option("-c", "--category", action="store_true", help="Compare categories", default=False)

    optparser.add_option("-i", "--instruments", action="store_true", help="Compare instruments by categories",
                         default=False)

    optparser.add_option("-s", "--statistics", action="store_true", help="Compare message statistics by instruments",
                         default=False)

    optparser.add_option("-t", "--totalstat", action="store_true", help="Show total statistics by categories",
                         default=False)

    optparser.add_option("-a", "--all", action="store_true", help="Show all information",
                         default=True)

    opt, args = optparser.parse_args()

    path_to_oldihe = opt.oldihe
    path_to_newihe = opt.newihe

    try:
        if not os.path.isfile(path_to_oldihe) or not os.path.isfile(path_to_newihe):
            print("Specified OLD/NEW files do not exist (-o and -n options)")
            sys.exit(1)
    except Exception:
        print('Please specify paths to existing OLD/NEW files (-o and -n options)')
        sys.exit(1)

    r = Reporter(path_to_oldihe, path_to_newihe)

    if opt.category or opt.instruments or opt.statistics or opt.totalstat:
        if opt.category:
            r.compare_categories()
        if opt.instruments:
            r.compare_instr_by_category()
        if opt.statistics:
            r.compare_stat()
        if opt.totalstat:
            r.total_counters()
    elif opt.all:
        r.compare_categories()
        r.compare_instr_by_category()
        r.compare_stat()
        r.total_counters()
    sys.exit(0)

if __name__ == '__main__':
    main()
